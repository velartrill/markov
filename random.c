#include <stdint.h>
#include <immintrin.h>
#if (!defined __amd64__) || defined _SOFTRAND
#include <time.h>
#endif

uint32_t seed32() {
	#if defined __amd64__ && !defined _SOFTRAND
		unsigned long long seed;
		asm volatile("rdrand %%rax; mov %%rax, %0" : "=rm" (seed) :: "memory", "rax");
		return seed;
	#else
		return time(NULL);
	#endif
}

uint32_t xorshift32(uint32_t state){
	uint32_t x = state;
	x ^= x << 13;
	x ^= x >> 17;
	x ^= x << 5;
	state = x;
	return x;
}
