#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "random.c"

const size_t // performance settings
	lookup_interval = 8,
	link_interval = 8;
	
typedef struct word {
	char* word;
	size_t linkc;
	struct word** links;
} word;

void addlink(word* w, word* link) {
	if (w -> linkc == 0) {
		w -> links = malloc(sizeof(word*)*link_interval);
	} else if (w -> linkc % link_interval == 0) {	
		w -> links = realloc(w -> links, sizeof(word*)*(w -> linkc + link_interval));
	}
	w->links[w -> linkc++] = link;
}

word** lookup[256] = { NULL };
size_t lookupsz[256] = { 0 };

word* store(const char* str) {
	uint8_t hash = str[0] ^ str[1];
	if (lookup[hash] == NULL) {
		lookup[hash] = malloc(sizeof(word)*lookup_interval);
		goto store;
	}
	for (size_t i = 0; i < lookupsz[hash]; ++i) {
		if(strcmp(lookup[hash][i] -> word, str) == 0) return lookup[hash][i];
	}
	if ((lookupsz[hash] % lookup_interval) == 0) {
		lookup[hash] = realloc(lookup[hash], sizeof(word*)*(lookupsz[hash] + lookup_interval));
	}
	
	store:;
	size_t i = lookupsz[hash]++;
	lookup[hash][i] = malloc(sizeof(word));
	lookup[hash][i] -> word = strdup(str);
	lookup[hash][i] -> linkc = 0;
	lookup[hash][i] -> links = NULL;
	
	return lookup[hash][i];
}

bool isws(char c) {
	return (c == ' ' || c=='\n' || c=='\t');
}

int main(int argc, char** argv) {
	if(argc!=3) {
		fprintf(stderr, "\x1b[1musage:\x1b[0m cat \x1b[32mcorpus\x1b[0m | %s \x1b[32maverage-words-per-line number-of-lines\x1b[0m\n",argv[0]);
		exit(1);
	}
	
	const size_t interval = 512*1000; // 512kb init buffer
	char* buf = malloc(interval);
	size_t len = 0, sz;
	for (size_t rd = 0; len += rd = fread(buf+len,1,interval,stdin), rd == interval;)
		buf = realloc(buf, sz+=interval);
	//fwrite(buf,1,len,stdout); fputc('\n', stdout);
	
	size_t cur = 0;
	word* prevwd = NULL;
		// possible opt: duplicate code to encode first word specially,
		// then begin loop, instead of checking for (prevwd != NULL)
		// in every iteration
	begin: {
		scan:
			if (cur == len) goto end;
			else if (isws(buf[cur])) { ++cur; goto scan;}
			// else goto encode;
		encode:;
			size_t l = 0;
			char wbuf[256]; //ehhhhhhh
			while(cur < len && !isws(buf[cur])) {
				wbuf[l] = buf[cur++];
				++l;
				if (l==256) { printf("bailing, impossible word\n"); exit(1); }
			}
			wbuf[l]=0;
			word* wd = store(wbuf);	
			if (prevwd != NULL) {				 
				addlink(prevwd,wd);
			}
			if (cur < len) {prevwd = wd; goto scan;}
	} end:;
	
	uint32_t seed = seed32();
	#define rnd (seed=xorshift32(seed))
	const uint8_t avglen = atoi(argv[1]);
	const uint8_t count = atoi(argv[2]);
	for (size_t i = 0; i<count; ++i) {
		uint8_t hash; 
		while(lookupsz[hash = rnd % 256] == 0);
			// this is unfortunate but better than caching a second copy
			// of the entire word database
		word* wd = lookup[hash][rnd%lookupsz[hash]];
		printf("%s ",wd->word);
		
		while (wd->linkc > 0 && rnd%avglen != 0) {
			wd = wd->links[rnd%wd->linkc];
			printf("%s ",wd->word);
		}
		printf("\n");
	}
	#undef rnd
}
